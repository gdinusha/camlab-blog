<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'blog-camlab');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'PH|wvejPZcREcj3f8X#l^.:1FcXa:MEkcv>B(,^-<:[EFf98{M:1:/Y  ?#zY%b`');
define('SECURE_AUTH_KEY',  '~Gj~4G3.0P}0pU)kE%mOpz(^tJUWaXS]nY4PQDGw%*JUrG|;-kl){L5JLW7r1kN,');
define('LOGGED_IN_KEY',    'zY.8]&Z)MI2@*nLx5wy^x#3LlrIRp&PwIpf(@z^sC,GtOhW-7I[VqIPImX]FfI2C');
define('NONCE_KEY',        '<&:vwa{{5`~gd.nus)1q;yz:1K)_4Tl_4 DfFs;(3Mp0B?;;nl8$5odRM@<Wvhcp');
define('AUTH_SALT',        'N7{-o0=wUpmo.yH<cKcqd|}IC`7o8v#H)(;-J@3cFPcrj4z)|2HVMI[wFEGGgmtk');
define('SECURE_AUTH_SALT', 'NmO6;=5IL}m:4 @&U]~=;K!la1T0,Phky=|^X>K6ZRVG<Z0@$j[kSTj8mD96T:A ');
define('LOGGED_IN_SALT',   '.r;PzS!J$2y{W@S>1hv~$ijJfFQl3&t3wUw::$KI 2ax6Tj4LQ,?:Lum6*%24Q6t');
define('NONCE_SALT',       'R~)Pu_]~,j(mkAm3)AnEuv/*#n2rgeL;Axx0<,q[LC^~[B){Ig(ATSm)=&DoR@3L');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'cmlb_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
