	<?php
	/**
	 * The template for displaying all pages.
	 *
	 * This is the template that displays all pages by default.
	 * Please note that this is the WordPress construct of pages
	 * and that other 'pages' on your WordPress site may use a
	 * different template.
	 *
	 * @link https://codex.wordpress.org/Template_Hierarchy
	 *
	 * @package bowdenmoss
	 */
	?>
	<?php get_header(); ?>

	<?php   get_template_part( 'page-includes/inc', 'headerbanner' ); ?>

	<div class="container min-body-hieght">

		<div class="row">
			<div class="col-xs-12 col-sm-12">
				<div class="bm-bread-crumbs">
					<?php custom_breadcrumbs(); ?>
				</div> 
			</div>
		</div>

		<?php while ( have_posts() ) : the_post(); ?>
			<?php if (get_the_content()) :?> 
				<div class="row">
					<div class="col-xs-12 col-sm-12">
						<div class="top-para">
							
							<?php the_content(); ?>
							
						</div> 
					</div>
				</div>
			<?php endif;?>
		<?php endwhile; // End of the loop. ?>
	</div>


	<?php get_footer(); 