<?php
/**
 * The template for displaying archive pages.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package bowdenmoss
 */

get_header(); ?>

<!-- 	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main"> -->
<section>
    <div class="container">
      <div class="row">
	<?php
		if ( have_posts() ) : ?>

			<header class="page-header">
				<?php
					the_archive_title( '<h1 class="page-title">', '</h1>' );
					the_archive_description( '<div class="taxonomy-description">', '</div>' );
				?>
			</header><!-- .page-header -->


        <div class="col-xs-12 col-sm-3 col-sm-push-9 col-md-3 col-md-push-9 col-lg-3 col-lg-push-9">
          <div class="blogs-main-r-items">
              <ul class="list-pest">

            <?php 
              $categories = get_categories( array(
                              'orderby' => '',
                              'order'   => 'ASC'
                          ) );
 
                  foreach( $categories as $category ) {
                    if(strtolower($category->name)!="uncategorised"):
                        ?>

                         <li>
                          <a href="<?php echo get_category_link( $category->term_id) ?>">
                          <?php echo esc_html( $category->name ) ?>
                        </a> 
                        <span><?php echo $category->count ; ?></span>
                        </li>

                         <?php
                       endif;
                              } 
                          ?>
            </ul>
          </div>
    <?php $home_right_image = get_field('home_right_image',get_option('page_on_front')); ?>
          <div class="blogs-main-r-items">
            <img class="img-responsive" src="<?php echo $home_right_image['url'];?>" alt="<?php echo $home_right_image['alt'];?>" title="<?php echo $home_right_image['title'];?>">
          </div>
        </div>
        <div class="col-xs-12 col-sm-9 col-sm-pull-3 col-md-9 col-md-pull-3 col-lg-9 col-lg-pull-3">

	

			<?php
			/* Start the Loop */
			while ( have_posts() ) : the_post(); 
				?>
		<div class="blog-one">
            <div class="row">
              <div class="col-xs-12 col-sm-6 col-sm-push-6 col-md-6 col-lg-6">
                <div class="right-banner">
  <?php $custom_featured_image = get_field('custom_featured_image',$post->ID); ?>
  <?php $custom_featured_image_mobile = get_field('custom_featured_image_mobile',$post->ID); ?>
                  <img class="img-responsive hidden-xs" src="<?php echo $custom_featured_image['url'];?>" alt="<?php echo $custom_featured_image['alt'];?>" title="<?php echo $custom_featured_image['title'];?>">

                  <img class="img-responsive visible-xs" src="<?php echo $custom_featured_image_mobile['url'];?>" alt="<?php echo $custom_featured_image_mobile['alt'];?>" title="<?php echo $custom_featured_image_mobile['title'];?>">
                </div>
              </div>

              <div class="col-xs-12 col-sm-6 col-sm-pull-6 col-md-6 col-lg-6 ">
                <div class="blog-deails">
                  <p class="we-lives">
                    <?php echo the_title(); ?>
                  </p>
                  <p class="second-hd"><?php echo get_the_date('j M Y'); ?>  / <span><?php echo get_the_author(); ?> </span></p>
              <?php $postshortdescription = get_field('post_short_description',$post->ID); ?>
                  <p class="it-has">
                    <?php echo $postshortdescription;?>
                  </p>
                  <a class="re-mores" href="<?php echo the_permalink(); ?>">READ MORE</a>
                </div>
              </div>

            </div>
        </div>


			<?php
				/*
				 * Include the Post-Format-specific template for the content.
				 * If you want to override this in a child theme, then include a file
				 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
				 */
				get_template_part( 'template-parts/content', get_post_format() );

			endwhile;

			the_posts_navigation();

		else :

			get_template_part( 'template-parts/content', 'none' );

		endif; ?>
	</div>
</div>
</div>
</section>


		<!-- </main> -->
		<!-- #main -->
	<!-- </div> -->
	<!-- #primary -->

<?php
get_sidebar();
get_footer();
