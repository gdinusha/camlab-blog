<?php
/* 
	Template Name: Site Map Page 
*/
  ?>
   <?php 
  $header_image = get_field('header_image',$post->ID); 
  $header_image_mobile = get_field('header_image_mobile',$post->ID);
  
  ?>

<?php get_header(); ?>

 <div class="bm-top-banner top-banner-white hidden-xs">
      <div>
       <img class="img-responsive" src="<?php echo  $header_image['url']; ?>" alt="<?php echo  $header_image['alt']; ?>" title="<?php echo  $header_image['title']; ?>">
        <div class="banner-text">
          <div class="container">
           <p class="black"><?php echo setPageTitles(); ?></p>
          </div>
        </div>
      </div>
    </div>

    <div class="bm-top-banner top-banner-white-xs visible-xs">
      <div>
        <img class="img-responsive" src="<?php echo  $header_image_mobile['url']; ?>" alt="<?php echo  $header_image_mobile['alt']; ?>" title="<?php echo  $header_image_mobile['title']; ?>">
        <div class="banner-text">
          <div class="container">
           <p class="black"><?php echo setPageTitles(); ?></p>
          </div>
        </div>
      </div>
    </div>

  <div class="container"> 
    <div class="row">
        <div class="col-xs-12 col-sm-12">
          <div class="bm-bread-crumbs">
            <?php custom_breadcrumbs(); ?>
          </div> 
        </div>
      </div>

      <?php while ( have_posts() ) : the_post(); ?>
    <?php if (get_the_content()) :?> 
      <div class="row">
        <div class="col-xs-12 col-sm-12">
          <div class="top-para">
           
           <?php the_content(); ?>
          
          </div> 
        </div>
      </div>
       <?php endif;?>
      <?php endwhile; // End of the loop. ?>

    <div class="row">
      <div class="col-xs-12">
        <div class="caree-main-pera">
          <?php echo do_shortcode('[SiteMap]');?>
        </div>
      </div>
    </div>
  </div>


<?php get_footer(); ?>
