<?php
/* 
	Template Name: Site News Page 
*/
?>
 <?php get_header(); ?>


  <?php 
  get_template_part( 'page-includes/inc', 'headerbanner' ); 

  ?>


    <div class="container">

      <div class="row">
        <div class="col-xs-12 col-sm-12">
          <div class="bm-bread-crumbs">
            <?php custom_breadcrumbs(); ?>
          </div> 
        </div>
      </div>

         <?php
         $paging_size=get_field("paging_size",$post->ID);
          $btpgid=get_queried_object_id();
          $btmetanm=get_post_meta( $btpgid, 'WP_Catid','true' );
          $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;

          $args = array( 'posts_per_page' => $paging_size, 'category_name' => 'news','paged' => $paged,'post_type' => 'post' );
          $postslist = new WP_Query( $args )?>

      <div class="row">
        <div class="col-xs-12 col-sm-12">
          <div class="bm-news-table">

            <div class="table-head-row">
              <div class="date-col"><p class="hidden-xs">DATE</p><p class="visible-xs">Details</p></div>
              <div class="info-col"><p  class="hidden-xs">TITLE</p></div>
              <div class="btn-col"><p  class="hidden-xs"></p></div>
            </div>
            <?php
          if ( $postslist->have_posts() ) :
            $i=0;
             while ( $postslist->have_posts() ) : $postslist->the_post(); 

            ?>
            <div class="table-row<?php echo ($i%2 == 1)? '2':'1';?>">
              <div class="date-info"><p><?php echo get_the_date('j S  M Y'); ?></p></div>
              <div class="detail-info"><p><?php echo the_title(); ?></p></div>
              <div class="view-btn"><a href="<?php echo the_permalink(); ?>">View More</a></div>
            </div>
            <?php $i++; endwhile;
               $big = 999999999; // need an unlikely integer
            $translated = __( ' &nbsp;', ' bowdenmoss' ); // Supply translatable string

            $args = array(
            'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
            'format' => '?paged=%#%',
            'current' => max( 1, get_query_var('paged') ),
            'total' => $postslist->max_num_pages,
            'prev_next'          => true,
            'prev_text'          => __('&lt;'),
            'next_text'          => __('&nbsp;&gt;'),
            'before_page_number' => '<span class="screen-reader-text">'.$translated.' </span>'
          ) ;

             ?>
            <div class="number-nav">
              <ul class="list-inline">
               <?php echo "<li>".paginate_links( $args )."</li>"; 
               ?>
              </ul>
            </div>


            <?php

            

           

         
            wp_reset_postdata();
          endif;
            ?>

          </div>
        </div>
      </div>
    </div>




<?php get_footer(); ?>
