<?php
/* 
	Template Name: Site Casestudy Filter Page 
*/
  ?>
<?php get_header(); ?>

    <?php 
  $header_image = get_field('header_image',$post->ID); 
  $header_image_mobile = get_field('header_image_mobile',$post->ID);
  ?>

    <!--//////////////////////// start of banner ////////////////////////// -->

    <div class="bm-top-banner top-banner-white  hidden-xs">
      <div>
       <img class="img-responsive" src="<?php echo  $header_image['url']; ?>" alt="<?php echo  $header_image['alt']; ?>" title="<?php echo  $header_image['title']; ?>">
        <div class="banner-text">
          <div class="container">
             <p class="black"><?php echo setPageTitles(); ?></p>
          </div>
        </div>
      </div>
    </div>

    <div class="bm-top-banner top-banner-white-xs visible-xs">
      <div>
        <img class="img-responsive" src="<?php echo  $header_image_mobile['url']; ?>" alt="<?php echo  $header_image_mobile['alt']; ?>" title="<?php echo  $header_image_mobile['title']; ?>">
        <div class="banner-text">
          <div class="container">
             <p class="black"><?php echo setPageTitles(); ?></p>
          </div>
        </div>
      </div>
    </div>


    <div class="container  min-body-hieght">

      <div class="row">
        <div class="col-xs-12 col-sm-12">
          <div class="bm-bread-crumbs">
             <?php custom_breadcrumbs(); ?>
          </div> 
        </div>
      </div>


      <?php $subpages = get_sub_pages();?>
      <div class="row">
        <div class="col-xs-12 col-sm-12">
          <div class="casestudy-main-box">
            <div class="box-row">

             <?php foreach ($subpages as $key => $value) {?>
          	<?php $image = get_field('custom_featured_image',$value->ID); ?>
              <div class="wrap">
                <div class="box-column">
                  <div class="casestudy-box">
                    <div class="blue-layer"></div>
                   <img class="img-responsive" src="<?php  echo $image['url'];?>" alt="<?php  echo $image['alt'];?>" src="<?php  echo $image['title'];?>">
                    <div class="casestudy-filter-title">
                      <p><?php echo $value->post_title; ?></p>
                      <h6><?php echo get_the_title(); ?></h6>
                      <a href="<?php echo get_the_permalink($value->ID); ?>" title="View All">View All</a>
                    </div>
                  </div>
                </div>
              </div>  
              <?php } ?>            
            </div>
          </div>
        </div>
      </div>

       
    </div>


<?php get_footer(); ?>
