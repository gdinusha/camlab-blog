<?php
/* 
	Template Name: Site Home Page 
*/
  ?>
  <?php get_header(); ?>
  <!--//////////////////////// start of banner ////////////////////////// -->

  <!-- ******************Content section*******************-->


   <?php   get_template_part( 'page-includes/inc', 'slider' ); ?>



    <!-- <div class="main-bnner-blog">
      <a href="#"><img class="img-responsive hidden-xs" src="<?php echo $headerimage['url'] ?>"></a>
      <a href="#"><img class="img-responsive visible-xs" src="<?php echo $headerimagemobile['url'] ?>"></a>

    </div>
  -->
<?php

  $args = array(
  'posts_per_page'   => 5,
  'offset'           => 0,
  'category'         => '',
  'category_name'    => '',
  'orderby'          => 'date',
  'order'            => 'DESC',
  'include'          => '',
  'exclude'          => '',
  'meta_key'         => '',
  'meta_value'       => '',
  'post_type'        => 'post',
  'post_mime_type'   => '',
  'post_parent'      => '',
  'author'     => '',
  'author_name'    => '',
  'post_status'      => 'publish',
  'suppress_filters' => true 
);
$postslist = new WP_Query( $args ); 
?>
  <section>
    <div class="container">
      <div class="row">
        <div class="col-xs-12 col-sm-3 col-sm-push-9 col-md-3 col-md-push-9 col-lg-3 col-lg-push-9">
          <div class="blogs-main-r-items">
              <ul class="list-pest">

            <?php 
              $categories = get_categories( array(
                              'orderby' => '',
                              'order'   => 'ASC'
                          ) );
 
                  foreach( $categories as $category ) {
                    if(strtolower($category->name)!="uncategorised"):
                        ?>

                         <li>
                          <a href="<?php echo get_category_link( $category->term_id) ?>">
                          <?php echo esc_html( $category->name ) ?>
                        </a> 
                        <span><?php echo $category->count ; ?></span>
                        </li>

                         <?php
                       endif;
                              } 
                          ?>
            </ul>
          </div>
    <?php $home_right_image = get_field('home_right_image',get_option('page_on_front')); ?>
          <div class="blogs-main-r-items">
            <img class="img-responsive" src="<?php echo $home_right_image['url'];?>" alt="<?php echo $home_right_image['alt'];?>" title="<?php echo $home_right_image['title'];?>">
          </div>
        </div>
        <div class="col-xs-12 col-sm-9 col-sm-pull-3 col-md-9 col-md-pull-3 col-lg-9 col-lg-pull-3">
  <?php
  //var_dump($postslist);
      if ( $postslist->have_posts() ) :
             while ( $postslist->have_posts() ) : $postslist->the_post(); 

            ?>
          <div class="blog-one">
            <div class="row">
              <div class="col-xs-12 col-sm-6 col-sm-push-6 col-md-6 col-lg-6">
                <div class="right-banner">
  <?php $custom_featured_image = get_field('custom_featured_image',$post->ID); ?>
  <?php $custom_featured_image_mobile = get_field('custom_featured_image_mobile',$post->ID); ?>
                  <img class="img-responsive hidden-xs" src="<?php echo $custom_featured_image['url'];?>" alt="<?php echo $custom_featured_image['alt'];?>" title="<?php echo $custom_featured_image['title'];?>">

                  <img class="img-responsive visible-xs" src="<?php echo $custom_featured_image_mobile['url'];?>" alt="<?php echo $custom_featured_image_mobile['alt'];?>" title="<?php echo $custom_featured_image_mobile['title'];?>">
                </div>
              </div>

              <div class="col-xs-12 col-sm-6 col-sm-pull-6 col-md-6 col-lg-6 ">
                <div class="blog-deails">
                  <p class="we-lives">
                    <?php echo the_title(); ?>
                  </p>
                  <p class="second-hd"><?php echo get_the_date('j M Y'); ?>  / <span><?php echo get_the_author(); ?> </span></p>
              <?php $postshortdescription = get_field('post_short_description',$post->ID); ?>
                  <p class="it-has">
                    <?php echo $postshortdescription;?>
                  </p>
                  <a class="re-mores" href="<?php echo the_permalink(); ?>">READ MORE</a>
                </div>
              </div>

            </div>
          </div>
      <?php 
    endwhile; 
    wp_reset_postdata();
    endif;
      ?>

        </div>

      </div>


    <div class="row ">
      <div class="col-xs-12 col-sm-9 col-md-9 col-lg-9">
        <div class="peginations lastg blog-onley">
          <a class="left-one hidden-xs" href="#"></a>
          <ul class="list-inline hidden-xs">
            <li><a href="#">1</a></li>
            <li><a href="#">2</a></li>
            <li><a href="#">3</a></li>
            <li><a href="#">4</a></li>
            <li><a href="#">5</a></li>
            <li><a href="#">6</a></li>
            <li><a href="#">7</a></li>
            <?php pagination(); 
               ?>
          </ul>
          <a class="right-two hidden-xs" href="#"></a>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- ******************eof Content section*******************-->


<?php get_footer(); ?>