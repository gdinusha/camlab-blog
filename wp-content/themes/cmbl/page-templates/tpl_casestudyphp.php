<?php
/* 
	Template Name: Site Casestudy Page 
*/
  ?>
<?php get_header(); ?>


  <!--//////////////////////// start of banner ////////////////////////// -->
<?php   get_template_part( 'page-includes/inc', 'headerbanner' ); ?>


    <div class="container">

      <div class="row">
        <div class="col-xs-12 col-sm-12">
          <div class="bm-bread-crumbs">
              <?php custom_breadcrumbs(); ?>
          </div> 
        </div>
      </div>

	<?php $subpages = get_sub_pages();?>

	
      <div class="row">
        <div class="col-xs-12 col-sm-12">
          <div class="casestudy-main-box">
            <div class="box-row">
           <?php foreach ($subpages as $key => $value) {?>
          	<?php $image = get_field('custom_featured_image',$value->ID); ?>
              <div class="wrap">
                <div class="box-column">
                  <div class="casestudy-box">
                    <div class="blue-layer"></div>
                     <img class="img-responsive" src="<?php  echo $image['url'];?>" alt="<?php  echo $image['alt'];?>" title="<?php  echo $image['title'];?>">
                    <div class="casestudy-title">
                      <p><?php echo $value->post_title; ?></p>
                       <a href="<?php echo get_the_permalink($value->ID); ?>" title="View All">View All</a>
                    </div>
                  </div>
                </div>
             </div>
           <?php } ?>


            </div>
          </div>
        </div>
      </div>

       
    </div>


<?php get_footer(); ?>
