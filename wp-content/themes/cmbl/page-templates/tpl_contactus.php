<?php
/* 
	Template Name: Site Contact Us Page 
*/
?>
<?php
    if ( function_exists( 'wpcf7_enqueue_scripts' ) ) {
        wpcf7_enqueue_scripts();
    }
 
    if ( function_exists( 'wpcf7_enqueue_styles' ) ) {
        wpcf7_enqueue_styles();
    }
?>

<?php get_header(); ?>

      <?php 
       $contact_address= get_field('contact_info_address',$post->ID);
      $contact_details= get_field('contact_details',$post->ID);
      $social_media_links = get_field('social_media_links',$post->ID); 
      ?>

    <!--//////////////////////// start of banner ////////////////////////// -->

  <?php get_template_part( 'page-includes/inc', 'headerbanner' ); ?>


    <div class="container">

      <div class="row">
        <div class="col-xs-12 col-sm-12">
          <div class="bm-bread-crumbs">
            <?php custom_breadcrumbs(); ?>
          </div> 
        </div>
      </div>

      <div class="row">
        <div class="bm-contact-box">
          <div class="col-xs-12 col-sm-6 col-sm-push-6 col-md-6 col-md-push-6 col-lg-5 col-lg-push-6">
            <div class="bm-info-box">
              <div class="info-head">
                <p><?php echo setPageTitles(); ?></p>
              </div>

               
              <div class="contact-info">
              <?php while ( have_posts() ) : the_post(); ?>
  				<?php if (get_the_content()) :?> 
  					<div class="main-para">
                	 <?php the_content(); ?>
            </div>
                   <p class="sub-para">We look forward to hearing from you. </p>
                	
                <?php endif;?>
  			<?php endwhile; // End of the loop. ?>

                <div class="details">
                  <div class="detail-row">
                    <p class="info-head-top"><span>bowden</span> moss Ltd</p>
                    <div class="info-part">
                      <div class="icon-box">
                        <img src="<?php echo get_template_directory_uri()?>/images/icons/location-icon.png" alt="location-icon" title="location-icon">
                      </div>
                      <div class="text-box-top">
                        <ul>
                         <?php foreach ($contact_address as $key => $value):?>
                            <li><?php echo $value['address_line'];  ?></li>
                          <?php endforeach; ?>
                        </ul>
                      </div>
                    </div>
                  </div>
   
                  <?php foreach ($contact_details as $key => $value):?>
                  <div class="detail-row">
                    <p class="info-head-other"><?php echo $value['title']; ?> :</p>
                    <div class="info-part">
                      <div class="icon-box">
                        <img src="<?php echo $value['phon_number_icon']['url']; ?>" alt="<?php echo $value['phon_number_icon']['alt']; ?>" title="<?php echo $value['phon_number_icon']['title']; ?>">
                      </div>
                      <div class="text-box-other">
                        <ul>
                          <li><a href="tel:+44<?php echo $value['number']; ?>" title="<?php echo $value['number']; ?>"><?php echo '0'.$value['number']; ?></a></li>
                        </ul>
                      </div>
                      </div>
                      <?php if (!empty($value['email'])): ?>
                        <div class="info-part">
                      <div class="icon-box">
                       <img src="<?php echo $value['email_icon']['url']; ?>" alt="<?php echo $value['email_icon']['alt']; ?>" title="<?php echo $value['email_icon']['title']; ?>">
                      </div>
                      <div class="text-box-other">
                        <ul>
                          <li> <a href="mailto:<?php echo $value['email']; ?>" title="<?php echo $value['email']; ?>"><?php echo $value['email']; ?></a></li>
                        </ul>
                      </div>
                   </div>
                  <?php endif; ?>
                    
                  </div>
                 <?php endforeach; ?>

                  
                  <div class="social-row">
                  <ul class="right list-inline">
                    <li>Follow us on</li>

                    <?php foreach ($social_media_links as $key => $value): ?>
                   <?php if ($value['social_media_link_url']) : ?>
 
                    <li>
                    <a href="<?php echo $value['social_media_link_url'] ?>" title="<?php echo $value['social_media_link_image']['title'];?>" target="_blank">
                    <img src="<?php echo $value['social_media_link_image']['url'];?>" alt="<?php echo$value['social_media_link_image']['alt'];?>" title="<?php echo $value['social_media_link_image']['title'];?>">
                    </a>
                    </li>
                     <?php endif; ?>
                  <?php endforeach; ?>
                    </ul>
                    </div>
                </div>
              </div>
            </div>
          </div>
          <div class="hidden-xs hidden-sm hidden-md col-lg-1"></div>
          <div class="col-xs-12 col-sm-6 col-sm-pull-6 col-md-6 col-md-pull-6 col-lg-5 col-lg-pull-6">
            <div class="bm-emailus-box">
              <div class="email-head">
                <p>EMAIL US</p>
              </div>
              <div class="email-form">

                <?php echo do_shortcode('[contact-form-7 id="4" title="Contact form 1" html_class="form-horizontal"]'); ?>

              </div>
            
            </div>
          </div>
          <div class="hidden-xs hidden-sm hidden-md col-lg-1"></div>
        </div>
      </div>

      



    </div>


<?php get_footer(); ?>