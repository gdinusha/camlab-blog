<?php
/* 
	Template Name: Site Casestudy Inner Page 
*/
  ?>
<?php get_header(); ?>


    <?php 
  $header_image = get_field('header_image',$post->ID); 
  $header_image_mobile = get_field('header_image_mobile',$post->ID);
  $slider_image = get_field('case_studty_inner_slider',$post->ID);
  $pdf_file = get_field('upload_pdf',$post->ID);
  ?>

 <!--//////////////////////// start of banner ////////////////////////// -->

    <div class="bm-top-banner top-banner-white hidden-xs">
      <div>
       <img class="img-responsive" src="<?php echo  $header_image['url']; ?>" alt="<?php echo  $header_image['alt']; ?>" title="<?php echo  $header_image['title']; ?>">
        <div class="banner-text">
          <div class="container">
           <p class="black"><?php echo setPageTitles(); ?></p>
          </div>
        </div>
      </div>
    </div>

    <div class="bm-top-banner top-banner-white-xs visible-xs">
      <div>
        <img class="img-responsive" src="<?php echo  $header_image_mobile['url']; ?>" alt="<?php echo  $header_image_mobile['alt']; ?>" title="<?php echo  $header_image_mobile['title']; ?>">
        <div class="banner-text">
          <div class="container">
           <p class="black"><?php echo setPageTitles(); ?></p>
          </div>
        </div>
      </div>
    </div>


    <div class="container min-body-hieght">

      <div class="row">
        <div class="col-xs-12 col-sm-12">
          <div class="bm-bread-crumbs">
               <?php custom_breadcrumbs(); ?>
          </div> 
        </div>
      </div>

     

      <div class="row">
        <div class="col-xs-12 col-sm-12">
          <div class="top-back-btn">
          	<div class="button">
            <?php  wps_parent_post(); ?> 
            </div>
          </div>
        </div>
      </div>


 
      <div class="row">
        <div class="col-xs-12 col-sm-12">
          <div class="bm-project-box">
            <div class="row">
              <div class="col-xs-12 col-sm-6">
                <div class="slider-box">
                  <div class="bm-detail-slider">
                    <?php if (!empty($slider_image )):?>
                   <?php foreach ($slider_image as $key => $value):?>
                  <div><img src="<?php echo $value['slider_image']['url'];?>" alt="<?php echo $value['slider_image']['alt'];?>" title="<?php echo $value['slider_image']['title'];?>"></div>
                    <?php endforeach; ?>

                  </div>
                  <div class="bm-btm-nav hidden-xs">
                   <?php foreach ($slider_image as $key => $value):?>
                    <div><img class="img-responsive" src="<?php echo $value['thumb_image']['url'];?>" alt="<?php echo $value['thumb_image']['alt'];?>" title="<?php echo $value['thumb_image']['title'];?>"></div>
                    <?php endforeach; ?>
                    <?php endif; ?>
                  </div>
                   
                  <div class="slider-btm-btns hidden-xs">
                    <?php get_template_part( 'page-includes/inc', 'subpage-paging' ); ?>
                  </div>

                </div>
              </div>
              <div class="col-xs-12 col-sm-6">
                <div class="content-box">
                  <div class="content-head">
                    <p><span><?php echo setPageTitles(); ?></span></p>
                  </div>

                <?php while ( have_posts() ) : the_post(); ?>
  				<?php if (get_the_content()) :?> 
                  <div class="content-para">
                     <?php the_content(); ?>
                  </div>
                  <?php endif;?>
  				<?php endwhile; // End of the loop. ?>

  				<?php $info = get_field('case_study_innerinfo',$post->ID); ?>
                  <div class="content-list">
                    <ul>
                     <?php if (!empty($info)):?>
                    <?php foreach ($info as $key => $value):?>
                    	<li><?php echo $value['title'];?> : <?php echo $value['text'];?></li>
                      <?php endforeach; ?>
                       <?php endif;?>
                    </ul>
                  </div>

                  <div class="content-btns">
                     <a class="contact" href="<?php echo site_url(); ?>/contact.html">Contact Us</a> 
                     <?php if(!empty($pdf_file['url'])) : ?>
                    <a class="download" href="<?php echo $pdf_file['url'];?>" title="Download PDF File" target="_blank"><span><img src="<?php echo get_template_directory_uri()?>/images/icons/download-icon.png" alt="Download PDF File" title="Download PDF File"></span>Download PDF File</a>
                  <?php endif; ?>
                  </div>
                </div>
              </div>
            </div>

            <div class="slider-btm-btns visible-xs">

			 <?php get_template_part( 'page-includes/inc', 'subpage-paging' ); ?>
             
            </div>

          </div>
        </div>
      </div>

    </div>



<?php get_footer(); ?>