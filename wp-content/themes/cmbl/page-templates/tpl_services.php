<?php
/* 
	Template Name: Site Services Page 
*/
  ?>
  <?php get_header(); ?>

  <!--//////////////////////// start of banner ////////////////////////// -->


  <?php   get_template_part( 'page-includes/inc', 'headerbanner' ); ?>



  <div class="container">

    <div class="row">
      <div class="col-xs-12 col-sm-12">
        <div class="bm-bread-crumbs">
         <?php custom_breadcrumbs(); ?>
       </div> 
     </div>
   </div>

   <?php while ( have_posts() ) : the_post(); ?>
     <?php if (get_the_content()) :?> 
      <div class="row">
        <div class="col-xs-12 col-sm-12">
          <div class="top-para">
           <?php the_content(); ?>
         </div>
         <div class="top-para2">
          <!-- <p>Bowden Moss can carry out a range of services in the commercial or domestic markets including:</p> -->
        </div>
      </div>
    </div>
  <?php endif;?>
<?php endwhile; // End of the loop. ?>

<?php $subpages = get_sub_pages();?>
<div class="row">
  <div class="bm-services-box">
    <div class="col-xs-12 col-sm-12">


      <div class="box-row">
        <div class="row">
          <?php foreach ($subpages as $key => $value) {?>
          <?php $image = get_field('custom_featured_image',$value->ID); ?>
	 <a href="<?php echo get_the_permalink($value->ID); ?>" title="<?php  echo $image['title']; ?>">
          <div class="col-xs-12 col-sm-4 service-box-wrap">
            <div class="service-box">
              <img class="img-responsive" src="<?php echo $image['url']; ?>" alt="<?php  echo $image['alt']; ?>" title="<?php  echo $image['title']; ?>">
              <div class="box-title">
                <p><?php echo $value->post_title; ?></p>
              </div>
            </div>
          </div>
	</a>
          <?php } ?>
        </div>
      </div>


    </div>
  </div>
</div>

<?php $standard_logo= get_field('standard_logos',$post->ID);?>
<div class="row">
  <div class="col-xs-12 col-sm-12">
    <div class="service-btm-slider">
      <ul class="list-inline">
        <?php foreach ($standard_logo as $key => $value):?>
          <li><img class="img-responsive" src="<?php echo $value['standard_logo']['url'] ?>" alt="<?php echo $value['standard_logo']['alt'] ?>" title="<?php echo $value['standard_logo']['title'] ?>"></li>
        <?php endforeach; ?>
      </ul>
    </div>
  </div>
</div>

</div>




<?php get_footer(); ?>
