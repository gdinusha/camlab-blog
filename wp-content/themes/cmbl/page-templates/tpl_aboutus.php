<?php
/* 
	Template Name: Site About Us Page 
*/
 ?>
 <?php get_header(); ?>

<?php   get_template_part( 'page-includes/inc', 'headerbanner' ); ?>


    <div class="container">

      <div class="row">
        <div class="col-xs-12 col-sm-12">
          <div class="bm-bread-crumbs">
          <?php custom_breadcrumbs(); ?>
          </div> 
        </div>
      </div>


    <?php while ( have_posts() ) : the_post(); ?>
  	<?php if (get_the_content()) :?> 
      <div class="row">
        <div class="col-xs-12 col-sm-12">
          <div class="top-para">
           
  				 <?php the_content(); ?>
          
          </div> 
        </div>
      </div>
       <?php endif;?>
  		<?php endwhile; // End of the loop. ?>

      <div class="row">
        <div class="bm-about-sec">
          <div class="col-xs-12 col-sm-6">
            <div class="about-img-box">
              <img class="img-responsive" src="<?php echo get_field('about_image',$post->ID)['url'];?>" alt="about-image" title="<?php echo get_field('about_image',$post->ID)['title'];?>">
            </div>
          </div>

          <?php $about_info = get_field('about_info',$post->ID); ?>
          <div class="col-xs-12 col-sm-6">
            <div class="about-info-box">
              <ul>
              <?php foreach ($about_info as $key => $value):?>
                <li><p> <img src="<?php echo $value['image']['url'];?>" alt="<?php echo $value['image']['alt'];?>" title="<?php echo $value['image']['title'];?>"><?php echo $value['text'];?></p></li>
                <?php endforeach; ?>
              </ul>
            </div>
          </div>
        </div>
      </div>

      



    </div>

<?php get_footer(); ?>