<?php $slider = get_field('blog_main_banner_images',get_option('page_on_front')); ?>

<section>
   <?php foreach ($slider as $key => $value):?>
  <div class="main-bnner-blog">
    <a href="#">
      <img class="img-responsive hidden-xs" src="<?php echo $value['blog_main_banner_images_desktop']['url'];?>" alt="<?php echo $value['blog_main_banner_images_desktop']['alt'];?>" title="<?php echo $value['blog_main_banner_images_desktop']['title'];?>">
    </a>
    <a href="#">
      <img class="img-responsive visible-xs" src="<?php echo $value['blog_main_banner_images_mobile']['url'];?>" alt="<?php echo $value['blog_main_banner_images_mobile']['alt'];?>" title="<?php echo $value['blog_main_banner_images_mobile']['title'];?>">
    </a>
  </div>
    <?php endforeach; ?>
 </section>

