   <?php $quicklinks = get_field('home_quick_links',get_option('page_on_front')); ?>

   <div class="row">
        <div class="col-xs-12 col-sm-12">
          <div id="info-boxes" class="top-box-row">
          <?php foreach ( $quicklinks as $key => $value):?>
            <a href="<?php echo site_url()."/". $value['url'];?>" title="<?php echo $value['links_text'];?>"> 
				<div class="bm-top-box" style="background-image: url(<?php echo $value['image']['url'];?>)  ; background-position: center; ">
              <div class="title-box">
               <?php echo $value['links_text'];?>
              </div>
            </div>
		</a>
            <?php endforeach; ?>
          </div>  
        </div>
      </div>