
<?php $recent_post = get_latest_post(2); ?>

 <div class="col-xs-12 col-sm-6">
            <div class="bm-news-box">
              <div class="news-title">
                <p>LATEST NEWS</p>
              </div>
              <div class="news-sec">
              <?php foreach ($recent_post as $key => $value): ?>
              	
              <?php $latst_news = get_field('custom_featured_image',$value['ID']); ?>

           
                <div class="news-row">
                  <div class="img-box">
                  
                    <img class="img-responsive" src="<?php echo $latst_news['url'] ?>" alt="<?php echo $latst_news['alt'] ?>" title="<?php echo $latst_news['title'] ?>">
                  </div>
                  <div class="news-info">
                    <p><?php echo $value['post_title']; ?></p>
                    <a class="button" href="<?php echo get_the_permalink($value['ID']); ?>" title="View More">View More</a>
                  </div>
                </div>
<?php endforeach ?>

              </div>
              <div class="view-all">
                <a class="button" href="<?php echo site_url(); ?>/news.html">View All News</a> 
              </div>
            </div>
          </div>