
    <?php 
  $header_image = get_field('custom_featured_image_large',$post->ID); 
  $header_image_mobile = get_field('custom_featured_image_mobile',$post->ID);
  ?>

    <div class="bm-top-banner hidden-xs">
      <div>
         <img class="img-responsive" src="<?php echo  $header_image['url']; ?>" alt="<?php echo  $header_image['alt']; ?>" title="<?php echo  $header_image['title']; ?>">
          <section class="hidden-xs">
            <div class="container">
              <div class="row">
                <div class="col-xs-12">
                  <div class="all-pagel-list blog-onla">
                    <?php custom_breadcrumbs(); ?>
                  </div>
                </div>
              </div>
            </div>
          </section>
        <div class="banner-text">
          <div class="container">
              <h1><?php echo setPageTitles(); ?></h1>
          </div>
        </div>
      </div>
    </div>

    <div class="bm-top-banner visible-xs">
      <div>
        <img class="img-responsive" src="<?php echo  $header_image_mobile['url']; ?>" alt="<?php echo  $header_image_mobile['alt']; ?>" title="<?php echo  $header_image_mobile['title']; ?>">
        <div class="banner-text">
          <div class="container">
              <h1><?php echo setPageTitles(); ?></h1>
          </div>
        </div>
      </div>
    </div>