    <?php 
   $contactus_image = get_field('contact_us_image',get_option('page_on_front')); 
   $contactus_desc = get_field('contact_us_description',get_option('page_on_front'));
   ?>

 <div class="col-xs-12 col-sm-6">
            <div class="home-contact-box" style="background-image: url(<?php echo $contactus_image['url'];?>); ">
              <div class="contact-info">
                <div class="title">
                  <p>CONTACT US</p>
                </div>
                <div class="info">
                  <p><?php echo $contactus_desc;?></p>
                  <a class="button" href="<?php echo site_url(); ?>/contact.html">Contact Now</a>
                </div>
              </div>
            </div>
          </div>