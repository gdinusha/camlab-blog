<?php 
  $footer_logo = get_field('footer_logo',get_option('page_on_front')); 
  $standard_logo = get_field('standard_logo',get_option('page_on_front')); 
  $social_media_links = get_field('social_media_links',get_option('page_on_front')); 
 ?>

<footer>
  <div class="footer-tip-top">
    <div class="container">
      <div class="row">
        <div class="col-xs-12">
          <div class="sub-scrip">
            <div class="row">
              <div class="col-xs-12 col-sm-12 col-md-12 col-lg-2"></div>
              <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3"><p class="hd-sub">SUBSCRIBE TO NEWSLETTER</p></div>
              <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4">
                <div class="input-sub">
                  <button class="bnt-input"><img src="images/footer-1.png"></button>
                  <input type="text" name="">
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="footer-link-section">
    <div class="container">
      <div class="row">

        <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
          <div class="links">
            <p class="hd">QUICK LINKS</p>
            <ul class="links-footer-ul"> 
              <li><a href="#">About</a></li>
              <li><a href="#">International</a></li>
              <li><a href="#">Sitemap</a></li>
              <li><a href="#">Cookie Policy</a></li>
              <li><a href="#">Terms & Conditions</a></li>
            </ul>
          </div>
        </div>
        <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
          <div class="links">
            <p class="hd">QUICK LINKS</p>
            <ul class="links-footer-ul"> 
              <li><a href="#">Careers</a></li>
              <li><a href="#">Exhibitions</a></li>
              <li><a href="#">Press</a></li>
              <li><a href="#">Delivery Information</a></li>
            </ul>
          </div>
        </div>
        <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
          <div class="links">
            <p class="hd">SERVICE</p>
            <ul class="links-footer-ul"> 
              <li><a href="#">Service</a></li>
              <li><a href="#">Live Support</a></li>
              <li><a href="#">Blog</a></li>
              <li><a href="#">Contact Us</a></li>
              <li><a href="#">Help</a></li>
            </ul>
          </div>
        </div>
        <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
          <div class="footer-r-last">
            <div class="camb">
              <a href="#"><img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/images/parts.png"></a>
            </div>
            <p class="contct">Contact</p>
            <p class="tp">Tel : +44 (0) 1954 233 145</p>
            <p class="tp">Fax : +44 (0) 1954 233 101</p>
            <div class="one-fh">
              <p class="emal">sales@industrialscientific.co.uk</p>
              <p class="emal">sales@camlab.co.uk</p>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-xs-12">
          <div class="footer-shoshal">
            <ul class="list-inline">
              <li><a href="#"><img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/images/pay-1.png"></a></li>
              <li><a href="#"><img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/images/pay-2.png"></a></li>
              <li><a href="#"><img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/images/pay-3.png"></a></li>
              <li><a href="#"><img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/images/pay-4.png"></a></li>
              <li><a href="#"><img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/images/pay-5.png"></a></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="center-footer">
    <div class="container">
      <div class="row">
        <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
          <div class="logo-snter"><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/images/center-logo-1.png"></a></div>
        </div>
        <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
          <div class="logo-snter2"><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/images/center-logo-2.png"></a></div>
        </div>
        <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
          <div class="logo-snter3"><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/images/center-logo-3.png"></a></div>
        </div>
        <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
          <div class="right-shoshal-last">
            <ul class="list-inline list-last-fooiter">
              <li><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/images/sh-1.png"></a></li>
              <li><a href="#"><img src="<?php echo get_template_directory_uri(); ?>images/sh-2.png"></a></li>
              <li><a href="#"><img src="<?php echo get_template_directory_uri(); ?>images/sh-3.png"></a></li>
              <li><a href="#"><img src="<?php echo get_template_directory_uri(); ?>images/sh-4.png"></a></li>
              <li><a href="#"><img src="<?php echo get_template_directory_uri(); ?>images/sh-5.png"></a></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="all-last">
    <div class="container">
      <div class="row">
        <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
          <div class="development">Developed by Onecreations</div>
        </div>
        <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
          <div class="coppy">
            Copyright ISS | camlab 2017 ©
          </div>
        </div>
      </div>
    </div>
  </div>
</footer>
        </div> <!-- END row-offcanvas-left -->
  </div> <!-- END offcanvas-wrapper -->