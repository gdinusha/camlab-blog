   <?php 
   $info_image = get_field('home_info_image',get_option('page_on_front')); 
   $info_desc = get_field('home_info_description',get_option('page_on_front'));
   ?>

    <div class="row">
        <div class="home-info-sec">
          <div class="col-xs-12 col-sm-5">
            <div class="content-img-box">
              <img class="img-responsive" src="<?php echo $info_image['url'];?>" alt="<?php echo $info_image['alt'];?>" title="<?php echo $info_image['title'];?>">
            </div>
          </div>
          <div class="col-xs-12 col-sm-7">
            <div class="content-info-box">
              <p><?php echo $info_desc;?></p>
            </div>
          </div>
        </div>
      </div>