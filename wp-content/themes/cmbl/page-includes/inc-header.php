<?php $main_logo = get_field('header_logo',get_option('page_on_front')); ?>
 <div class="offcanvas-wrapper">
        <div class="row-offcanvas row-offcanvas-left">

          <header>
            <div class="container">
              <div class="row">
                <div class="col-xs-12">
                  <div class="main-header">
                    <div class="row">
                      <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                        <div class="left-main-header">
                          <a href="#"><img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/images/main-logo.png"></a>
                          <p class="gogo-down">A camlab Group Company</p>
                        </div>
                      </div>
                      <div class="col-xs-12 col-sm-9 col-md-9 col-lg-9 mobile-position">
                        <div class="right-header">
                          <div class="row hidden-xs">
                            <div class="col-xs-12">
                              <div class="login-div">
                                <ul class="list-inline login-ul">
                                  <li><a href="#">LOG IN</a></li>
                                  <li>|</li>
                                  <li><a href="#">REGISTER</a></li>
                                </ul>
                              </div>
                            </div>
                          </div>
                          <div class="ser-line">
                            <div class="l-search">
                              <!-- <div class="search">
                                <button class="search-btn"><img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/images/header-icon-3.png"></button>
                                <input placeholder="Search" type="text" name="search">
                              </div> -->
                              <?php get_search_form(); ?>
                            </div>
                            <div class="r-search">
                              <div class="right-r blog-rjd">
                                
                                <a class="live" href="#"><span><img src="<?php echo get_template_directory_uri(); ?>/images/header-icon-1.png"></span>Live Chat</a>
                                <a class="call" href="#"><span><img src="<?php echo get_template_directory_uri(); ?>/images/header-icon-2.png"></span>+44 (0) 1954 233 145</a>
                              </div>
                            </div>
                            <div class="cart">
                              <a class="hamberger mobile-only m-ocl-btn pull-left visible-xs" href="#" data-toggle="offcanvas-left">
                                <span></span>

                              </a>

                              <a class="serc" href="#"><img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/images/mobile-all-search.png"></a>

                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </header>
          <section class="hidden-xs">
            <div class="main-menu">
              <div class="container">
                <div class="row">
                  <div class="col-xs-12">
                    <div class="menu-list">
                      <ul class="list-inline m-menu-ul">
                        <li><a href="home.html"><span><img src="<?php echo get_template_directory_uri(); ?>/images/home-icon.png"></span></a></li>
                        <li class="dropdown">
                          <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span> Products</span></a>
                          <div class="dropdown-menu main-bacro-menu">
                            <ul class="first-level">
                              <li class="dropdown dropdown-submenu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Cleaning</a>
                                <ul class="dropdown-menu second-level">
                                  <li><a href="#">Disinfectant</a></li>
                                  <li><a href="#">Paper Products</a></li>
                                  <li><a href="#">Cleaning Solutions</a></li>
                                  <li><a href="#">Soaps and Detergents</a></li>
                                </ul>
                              </li>
                              <li class="dropdown dropdown-submenu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Lab Consumables</a>
                                <ul class="dropdown-menu second-level">
                                  <li><a href="#">Aluminium Foli / Parafilm</a></li>
                                  <li><a href="#">Bags</a></li>
                                  <li class="dropdown dropdown-submenu">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Labelling / Tapes</a>
                                    <ul class="dropdown-menu third-level">
                                      <li class="dropdown dropdown-submenu">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Labels For Machines</a>
                                        <div class="dropdown-menu last-level">

                                          <div class="text">
                                            <img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/images/img-mega-menu.png">
                                          </div>
                                          <div class="left-names">
                                            <p class="power-name">
                                              330001 VIRKON 50gm <br>
                                              POWDER SACHETS
                                            </p>
                                            <p class="main-price">£ 24.00</p>
                                          </div>
                                          <p class="etxt">
                                            Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry’s standard dummy text ever
                                          </p>
                                        </div>
                                      </li>
                                      <li class="dropdown dropdown-submenu">
                                       <a href="#" class="dropdown-toggle" data-toggle="dropdown">Labels</a>
                                       <div class="dropdown-menu last-level">

                                        <div class="text">
                                          <img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/images/img-mega-menu.png">
                                        </div>
                                        <div class="left-names">
                                          <p class="power-name">
                                            440001 VIRKON 50gm <br>
                                            POWDER SACHETS
                                          </p>
                                          <p class="main-price">£ 34.00</p>
                                        </div>
                                        <p class="etxt">
                                          Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry’s standard dummy text ever
                                        </p>
                                      </div>
                                    </li>
                                    <li class="dropdown dropdown-submenu">
                                      <a href="#" class="dropdown-toggle" data-toggle="dropdown">Label Tapes</a>
                                      <div class="dropdown-menu last-level">
                                        <div class="text">text3333</div>
                                      </div>
                                    </li>
                                  </ul>
                                </li>
                                <li><a href="#">Surgical Supplies</a></li>
                              </ul>
                            </li>
                            <li><a href="#">Labware-Containers & Volumetrics</a></li>
                            <li><a href="#">PPE</a></li>
                            <li><a href="#">Safety</a></li>
                          </ul>
                        </div>
                      </li>
                      <li><a href="#"><span> Brands</span></a></li>
                      <li><a href="#"><span> Information</span></a></li>
                      <li><a href="#"><span> International</span></a></li>
                      <li><a href="#"><span> Clearance</span></a></li>
                      <li><a href="#"><span> Contact</span></a></li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>



        <div class="main-menu sidebar-offcanvas offcanvas-left visible-xs">
          <div class="header">
            <p class="left-name">MENU</p>
            <button class="accout-btn">ACCOUNT</button>
          </div>
          <div class="home-items">
            <div class="" id="accordion1">

              <div class="panel">
                <a data-toggle="collapse" class="mb-lavel-1" data-parent="#accordion1" href="#collapse1">Home</a>
                <div id="collapse1" class="panel-collapse collapse">
                </div>
              </div>

              <div class="panel ">
                <a data-toggle="collapse" class="mb-lavel-1" data-parent="#accordion1" href="#collapse2">Products <span></span></a>
                <div id="collapse2" class="panel-collapse collapse">
                  <div class="panel-body">

                    <div id="second" class="">

                      <div class="panel">
                       <a data-toggle="collapse" class="mb-lavel-2" data-parent="#second" href="#collapse2-1">Cleaning <span></span></a>
                       <div id="collapse2-1" class="panel-collapse collapse">
                       </div>
                     </div>
                     <div class="panel">
                       <a data-toggle="collapse" class="mb-lavel-2" data-parent="#second" href="#collapse2-2">Lab Consumables <span></span></a>
                       <div id="collapse2-2" class="panel-collapse collapse">
                        <div id="thrd" class="">

                          <div class="panel">
                            <a data-toggle="collapse" class="mb-lavel-3" data-parent="#thrd" href="#collapse3-1">Aluminium Foil / Parafilm</a>
                            <div id="collapse3-1" class="panel-collapse collapse">
                            </div>
                          </div>
                          <div class="panel">
                            <a data-toggle="collapse" class="mb-lavel-3" data-parent="#thrd" href="#collapse3-2">Bags</a>
                            <div id="collapse3-2" class="panel-collapse collapse">
                            </div>
                          </div>
                          <div class="panel">
                            <a data-toggle="collapse" class="mb-lavel-3" data-parent="#thrd" href="#collapse3-3">Labelling / Tapes <span></span></a>
                            <div id="collapse3-3" class="panel-collapse collapse">
                              <div id="forth" class="">
                                <div class="panel">
                                  <a data-toggle="collapse" class="mb-lavel-4" data-parent="#forth" href="#collapse4-2">Labels For Machines</a>
                                  <a data-toggle="collapse" class="mb-lavel-4" data-parent="#forth" href="#collapse4-2">Labels</a>
                                  <a data-toggle="collapse" class="mb-lavel-4" data-parent="#forth" href="#collapse4-2">Label Types</a>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="panel">
                            <a data-toggle="collapse" class="mb-lavel-3" data-parent="#thrd" href="#collapse3-4">Surgical Supplies</a>
                            <div id="collapse3-4" class="panel-collapse collapse">
                            </div>
                          </div>

                        </div>
                      </div>
                    </div>
                    <div class="panel">
                     <a data-toggle="collapse" class="mb-lavel-2" data-parent="#second" href="#collapse2-3">Labware - Containers & <br>
                       Volumetrics <span></span></a>
                       <div id="collapse2-3" class="panel-collapse collapse">
                       </div>
                     </div>
                     <div class="panel">
                       <a data-toggle="collapse" class="mb-lavel-2" data-parent="#second" href="#collapse2-4">PPE <span></span></a>
                       <div id="collapse2-4" class="panel-collapse collapse">
                       </div>
                     </div>
                     <div class="panel">
                       <a data-toggle="collapse" class="mb-lavel-2" data-parent="#second" href="#collapse2-5">Safety <span></span></a>
                       <div id="collapse2-5" class="panel-collapse collapse">
                       </div>
                     </div>

                   </div>

                 </div>
               </div>
             </div>

             <div class="panel ">
              <a data-toggle="collapse" class="mb-lavel-1" data-parent="#accordion1" href="#collapse3">Information <span></span></a>
              <div id="collapse3" class="panel-collapse collapse">
                <div class="panel-body"></div>
              </div>
            </div>
            <div class="panel ">
              <a data-toggle="collapse" class="mb-lavel-1" data-parent="#accordion1" href="#collapse4">International</a>
              <div id="collapse4" class="panel-collapse collapse">
                <div class="panel-body"></div>
              </div>
            </div>
            <div class="panel ">
              <a data-toggle="collapse" class="mb-lavel-1" data-parent="#accordion1" href="#collapse5">Clearance</a>
              <div id="collapse5" class="panel-collapse collapse">
                <div class="panel-body"></div>
              </div>
            </div>
            <div class="panel ">
              <a data-toggle="collapse" class="mb-lavel-1" data-parent="#accordion1" href="#collapse6">Contact</a>
              <div id="collapse6" class="panel-collapse collapse">
                <div class="panel-body"></div>
              </div>
            </div>
          </div> 
        </div>
      </div>

      <div class="mini-cart">
        <div class="min-cart-bacro"></div>
        <div class="min-items">
          <div class="container">
            <div class="row">
              <div class="col-xs-12">
                <div class="positionss">
                  <div class="mini-box">
<button class="close0-mobils visible-xs"><img src="images/close-miniu-s.png"></button>
                    <button class="cuntinu">CONTINUE SHOPPING</button>

                    <div class="sucses-items">
                      <button class="close-breen"><img src="images/close-greens.png"></button>
                      <p class="sus-p">Success !</p>
                      <p class="items-are">Item/s are successfully added.</p>
                    </div>

                    <div class="items-mini">
                     <a class="basket-b" href="#"><img src="images/basket-close-mini.png"></a>
                     <div class="left-product"><img src="images/mini-ct-product.png"></div>
                     <div class="felt">
                      <p class="name">FEIT Electric Eco - Bulb</p>
                      <p class="pricess">£ 240.00<span>excl. VAT</span></p>
                      <p class="pricess">£ 260.00<span>inc. VAT</span></p>
                    </div>
                  </div>
                  <p class="sub-totl">Sub Total Excl. VAT<span>£ 120.00</span></p>
                  <button class="view-basket">VIEW BASKET</button>
                  <button class="check">CHECKOUT</button>
                  <ul class="list-inline mii-last">
                    <li><img src="images/pay-images-1.png"></li>
                    <li><img src="images/pay-images-2.png"></li>
                  </ul>

                </div>
              </div>
            </div>

          </div>
        </div>
      </div>
    </div>
