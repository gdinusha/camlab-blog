
$(document).ready(function(){
$('.main-full-slider').slick({
  dots:true,
  responsive: [
      {
      breakpoint: 767,
      settings: {
        dots: false
      }
    }
  ]
});
$('.mobile-top-slider').slick({
	arrows:false,
	  slidesToShow: 3,
  slidesToScroll: 1,
    	dots: false,
	responsive: [
	{
	 breakpoint: 768,
	 settings: {
	 slidesToShow: 1,
	 slidesToScroll: 1,
	 autoplay: true,
	 autoplaySpeed: 2000
	}
	 }
	]
});
$('.home-categry-img').slick({
  dots: false,
  infinite: false,
  speed: 300,
  slidesToShow: 5,
  arrows:true,
  slidesToScroll: 1,
  responsive: [
    {
      breakpoint: 1199,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 3,
        infinite: true,
        arrows:true,
        dots: false
      }
    },
    {
      breakpoint: 600,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
  ]
});
$('.home-down').slick({
  dots: false,
  infinite: false,
  speed: 300,
  slidesToShow: 6,
  arrows:true,
  slidesToScroll: 1,
  responsive: [
    {
      breakpoint: 1199,
      settings: {
        slidesToShow: 4,
        slidesToScroll: 1,
        infinite: true,
        arrows:true,
        dots: false
      }
    },
    {
      breakpoint: 600,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
  ]
});


 $('.big-slider-detals').slick({
  slidesToShow: 1,
  slidesToScroll: 1,
  arrows: false,
  fade: true,
  asNavFor: '.small-de-slider',
  responsive: [
      {
      breakpoint: 767,
      settings: {
        arrows:false,
        dots: true
      }
    }
  ]
});
$('.small-de-slider').slick({
  slidesToShow: 5,
  slidesToScroll: 1,
  asNavFor: '.big-slider-detals',
  dots: false,
  arrows: true,
  centerMode: false,
  focusOnSelect: true
});



  jQuery(document).ready(function(){
    // This button will increment the value
    $('.qtyplus').click(function(e){
        // Stop acting like a button
        e.preventDefault();
        // Get the field name
        fieldName = $(this).attr('field');
        // Get its current value
        var currentVal = parseInt($('input[name='+fieldName+']').val());
        // If is not undefined
        if (!isNaN(currentVal)) {
            // Increment
            $('input[name='+fieldName+']').val(currentVal + 1);
        } else {
            // Otherwise put a 0 there
            $('input[name='+fieldName+']').val(0);
        }
    });
    // This button will decrement the value till 0
    $(".qtyminus").click(function(e) {
        // Stop acting like a button
        e.preventDefault();
        // Get the field name
        fieldName = $(this).attr('field');
        // Get its current value
        var currentVal = parseInt($('input[name='+fieldName+']').val());
        // If it isn't undefined or its greater than 0
        if (!isNaN(currentVal) && currentVal > 0) {
            // Decrement one
            $('input[name='+fieldName+']').val(currentVal - 1);
        } else {
            // Otherwise put a 0 there
            $('input[name='+fieldName+']').val(0);
        }
    });
});
$('.menu-accord').on('click', function(){
    $(this).next('div').toggle(650);
    
    var span = $(this).children('span');
   if (span.hasClass('sign-plus')){
         span.removeClass('sign-plus');
         span.addClass("sign-minus");
    } else {
         span.removeClass('sign-minus');
         span.addClass("sign-plus");
      }
});

});



/* Set the width of the side navigation to 250px and the left margin of the page content to 250px */
function openNav() {
    document.getElementById("mySidenav").style.width = "250px";
    document.getElementById("main").style.marginLeft = "250px";
}

/* Set the width of the side navigation to 0 and the left margin of the page content to 0 */
function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
    document.getElementById("main").style.marginLeft = "0";
}

jQuery( document ).ready(function( $ ) {

    // Trigger Left OffCanvas
    $('[data-toggle="offcanvas-left"]').click(function(event) {
        event.preventDefault();
        $('.row-offcanvas-left').toggleClass('active');
         $('.body').css({'overflow':'hidden'});
    });

    // Trigger Right OffCanvas
    $('[data-toggle="offcanvas-right"]').click(function(event) {
        event.preventDefault();
        $('.row-offcanvas-right').toggleClass('active');
    });

});

$('.hamberger').click(function(){
    $('.hamberger span').toggleClass('crose');
});

$(".two").click(function(){
    $(".mini-cart").addClass("minions");
});
$(".min-cart-bacro,.close0-mobils").click(function(){
    $(".mini-cart").removeClass("minions");
});
$(document).ready(function(){
    $( ".serc" ).click(function() {
      $( ".l-search" ).slideToggle( "slow" );
    });
});