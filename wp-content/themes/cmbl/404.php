<?php
/**
 * The template for displaying 404 pages (not found).
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package bowdenmoss
 */

get_header(); ?>

<div class="bm-top-banner hidden-xs">
      <div>
        <img class="img-responsive" src="<?php echo get_template_directory_uri()?>/images/banners/banner-404.png" alt="banner">
        <div class="banner-text">
          <div class="container">
            <p>Page not Found</p>
          </div>
        </div>
      </div>
    </div>

    <div class="bm-top-banner visible-xs">
      <div>
        <img class="img-responsive" src="<?php echo get_template_directory_uri()?>/images/banners/banner-404-xs.png" alt="banner">
        <div class="banner-text">
          <div class="container">
            <p>WE ARE SORRY, BUT THE PAGE YOU ARE LOOKING FOR CANNOT BE FOUND.</p>
          </div>
        </div>
      </div>
    </div>

<!-- ******************Content section*******************-->
<section>
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<div class="platform-main-hd">
					<h1 class="platform-main-hd-p"><?php _e( '404 - WE ARE SORRY, BUT THE PAGE YOU ARE LOOKING FOR CANNOT BE FOUND.', 'bowdenmoss' ); ?></h1>
				</div>
				<p>
						- If you typed the URL directly, please make sure the spelling is correct. If you clicked on a link to get here, we must have moved the content. 
</p>
<p>
- If you are not sure how you got here, Go Back to the previous page or return to our <a href="<?php echo site_url();?>/" title="Homepage">Homepage</a>.
</p>
			</div>
		</div>
		
		<?php while ( have_posts() ) : the_post(); ?>

			<div class="row">
				<div class="col-xs-12">
					<div class="caree-main-pera">
						<?php //the_content(); ?>
						
					</div>
				</div>
			</div>

		<?php endwhile; // End of the loop. ?>
		<div class="row">
			<div class="col-xs-12">
				<div class="caree-main-pera">
					<?php echo do_shortcode('[SiteMap]');?>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- Start Contact Details Section  -->
<?php get_template_part( 'page-includes/inc', 'contact-details' ); ?>
<!-- End Contact Details Section  -->

<!-- Start Technology Partners Section  -->
<?php get_template_part( 'page-includes/inc', 'technology-partners' ); ?>
<!-- End Technology Partners Section  -->

<!-- ******************eof Content section*******************-->
<?php
get_footer();
?>