<?php
/**
 * The template for displaying search results pages.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package bowdenmoss
 */

get_header(); ?>

<section>
	<div class="container">

	<header class="page-header">
		<?php if ( have_posts() ) : ?>
			<h1 class="page-title"><?php printf( __( 'Search Results for: %s', 'bowdenmoss' ), '<span>' . get_search_query() . '</span>' ); ?></h1>
		<?php else : ?>
			<h1 class="page-title"><?php _e( 'Nothing Found', 'bowdenmoss' ); ?></h1>
		<?php endif; ?>
	</header><!-- .page-header -->

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<?php
			if ( have_posts() ) :
				/* Start the Loop */
			while ( have_posts() ) : the_post();?>
			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

				<header class="entry-header">
					<?php if ( 'post' === get_post_type() ) : ?>
						<div class="entry-meta">
							<?php
							echo twentyseventeen_time_link();
							twentyseventeen_edit_link();
							?>
						</div><!-- .entry-meta -->
					<?php elseif ( 'page' === get_post_type() && get_edit_post_link() ) : ?>
						<div class="entry-meta">
							<?php twentyseventeen_edit_link(); ?>
						</div><!-- .entry-meta -->
					<?php endif; ?>

					<?php the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' ); ?>
				</header><!-- .entry-header -->

				<div class="entry-summary">
					<?php the_excerpt(); ?>
				</div><!-- .entry-summary -->

			</article><!-- #post-## -->
			<?php
			endwhile; // End of the loop.



			else : ?>

			<p><?php _e( 'Sorry, but nothing matched your search terms. Please try again with some different keywords.', 'bowdenmoss' ); ?></p>
			<?php
			get_search_form();

			endif;
			?>

		</main><!-- #main -->
	</div><!-- #primary -->
</section>

<!-- Start Contact Details Section  -->
<?php get_template_part( 'page-includes/inc', 'contact-details' ); ?>
<!-- End Contact Details Section  -->

<!-- Start Technology Partners Section  -->
<?php get_template_part( 'page-includes/inc', 'technology-partners' ); ?>
<!-- End Technology Partners Section  -->

<!-- ******************eof Content section*******************-->

<?php get_footer(); ?>