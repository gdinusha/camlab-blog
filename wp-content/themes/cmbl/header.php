<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package bowdenmoss
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<title><?php echo (is_404())? "Page Not Found":getPageTitles();?></title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="robots" content="NOINDEX,NOFOLLOW">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="<?php echo set_description(); ?>"/>
<meta name="keywords" content="<?php echo set_keywords(); ?>"/>
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<meta name="SKYPE_TOOLBAR" content="SKYPE_TOOLBAR_PARSER_COMPATIBLE" />

<link rel="shortcut icon" href="<?php echo get_template_directory_uri();?>/favicon.ico">
  <!-- Bootstrap -->
  <link href="<?php echo get_template_directory_uri(); ?>/css/bootstrap.min.css" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/slick/slick.css"/>
  <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/slick/slick-theme.css"/>
  <link href="<?php echo get_template_directory_uri(); ?>/css/style.css" rel="stylesheet">

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
<?php wp_head(); ?>
<?php //echo //set_header_tracking_codes(); ?>
</head>
<body <?php body_class('main-padding'); ?>>
<?php //echo set_body_tracking_codes(); ?>
 <?php   get_template_part( 'page-includes/inc', 'header' ); ?>


  