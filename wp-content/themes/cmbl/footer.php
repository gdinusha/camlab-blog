<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package bowdenmoss
 */

?>

<?php   get_template_part( 'page-includes/inc', 'footer' ); ?>


	
   <?php wp_footer(); ?>
  <?php echo do_shortcode('[setEditBar]'); ?>
  <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script> -->
<script src="<?php echo get_template_directory_uri(); ?>/js/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="<?php echo get_template_directory_uri(); ?>/js/bootstrap.min.js"></script>
<script type="text/javascript" src="slick/slick.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/ie-support.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/custom.js"></script>
 </body>
</html>