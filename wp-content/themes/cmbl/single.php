<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package bowdenmoss
 */

get_header(); ?>



<?php get_template_part( 'page-includes/inc', 'headerbanner' ); ?>

<div class="container min-body-hieght">

<section>
    <div class="container">
      <div class="row">
        <div class="col-xs-12 col-sm-3 col-sm-push-9 col-md-3 col-md-push-9 col-lg-3 col-lg-push-9">
        	<div class="blogs-main-r-items">
             	<ul class="list-pest">

            <?php 
              $categories = get_categories( array(
                              'orderby' => '',
                              'order'   => 'ASC'
                          ) );
 
                  foreach( $categories as $category ) {
                    if(strtolower($category->name)!="uncategorised"):
                        ?>

                <li>
                    <a href="<?php echo get_category_link( $category->term_id) ?>">
                          <?php echo esc_html( $category->name ) ?>
                    </a> 
                    <span><?php echo $category->count ; ?></span>
                </li>

                  	<?php
                  		endif;
                         } 
                    ?>
            	</ul>
          	</div>
    <?php $home_right_image = get_field('home_right_image',get_option('page_on_front')); ?>
          <div class="blogs-main-r-items">
            <img class="img-responsive" src="<?php echo $home_right_image['url'];?>" alt="<?php echo $home_right_image['alt'];?>" title="<?php echo $home_right_image['title'];?>">
          </div>
        </div>

        <?php while ( have_posts() ) : the_post(); ?>

        <div class="col-xs-12 col-sm-9 col-sm-pull-3 col-md-9 col-md-pull-3 col-lg-9 col-lg-pull-3">
        	<div class="top-para">

				<?php
					the_content();
				?>
        <p class="second-hd"><?php echo get_the_date('j M Y'); ?>  / <span><?php echo get_the_author(); ?> </span></p>
				<?php
				 // If comments are open or we have at least one comment, load up the comment template.
					//if ( comments_open() || get_comments_number() ) :
						//comments_template();
					//endif;
					 ?>

			</div>
        </div>

        <?php endwhile; // End of the loop. ?>

    </div>
</div>
</section>
 
				

	






</div>
<div id="primary" class="content-area">
	<main id="main" class="site-main">

		

	</main><!-- #main -->
</div><!-- #primary -->

<?php
get_sidebar();
get_footer();
