<?php
/**
 * camlab functions and definitions.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package camlab
 */

if ( ! function_exists( 'camlab_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function camlab_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on camlab, use a find and replace
	 * to change 'camlab' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'camlab', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => esc_html__( 'Primary', 'camlab' ),
		'footer' => esc_html__( 'Footer', 'camlab' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	/*
	 * Enable support for Post Formats.
	 * See https://developer.wordpress.org/themes/functionality/post-formats/
	 */
	add_theme_support( 'post-formats', array(
		'aside',
		'image',
		'video',
		'quote',
		'link',
	) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'camlab_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );
}
endif;
add_action( 'after_setup_theme', 'camlab_setup' );

add_action( 'init', 'my_add_excerpts_to_pages' );
function my_add_excerpts_to_pages() {
     add_post_type_support( 'page', 'excerpt' );
}

function my_deregister_scripts(){
  wp_deregister_script( 'wp-embed' );
}
add_action( 'wp_footer', 'my_deregister_scripts' );

function wpdocs_dequeue_script() {
        wp_dequeue_script( 'jquery' ); 
} 
add_action( 'wp_print_scripts', 'wpdocs_dequeue_script', 100 );

/**
 * Enqueue scripts and styles.
 */
function camlab_scripts() {
	wp_enqueue_style( 'bootstrap.min.css', get_template_directory_uri() . '/css/bootstrap.min.css' );

	wp_enqueue_style( 'slick.css', get_template_directory_uri() . '/css/slick.min.css' );

	wp_enqueue_style( 'slick-theme.css', get_template_directory_uri() . '/css/slick-theme.min.css' );

	wp_enqueue_style( 'fonts.css', get_template_directory_uri() . '/fonts/fonts.css' );

	wp_enqueue_style( 'camlab-style', get_stylesheet_uri() );

	wp_enqueue_script( 'jquery.min', get_template_directory_uri() . '/js/jquery.min.js', array(), '', true );

    wp_enqueue_script( 'bootstrap.min', get_template_directory_uri() . '/js/bootstrap.min.js', array(), '', true );

    wp_enqueue_script( 'slick.min', get_template_directory_uri() . '/js/slick/slick.min.js', array(), '', true );

	//wp_enqueue_script( 'slick', get_template_directory_uri() . '/js/slick/slick.js', array(), '', true );

	wp_enqueue_script( 'slider-script', get_template_directory_uri() . '/js/slider-script.js', array(), '', true );

	wp_enqueue_script( 'slider-slick', get_template_directory_uri() . '/js/slider-slick.js', array(), '', true );

	wp_enqueue_script( 'custom', get_template_directory_uri() . '/js/custom.js', array(), '', true );
	
}
add_action( 'wp_enqueue_scripts', 'camlab_scripts' );


// REMOVE WP EMOJI
remove_action('wp_head', 'print_emoji_detection_script', 7);
remove_action('wp_print_styles', 'print_emoji_styles');

remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
remove_action( 'admin_print_styles', 'print_emoji_styles' );


add_filter( 'wpcf7_load_js', '__return_false' );
add_filter( 'wpcf7_load_css', '__return_false' );


//Deletes all CSS classes and id's, except for those listed in the array below
function custom_wp_nav_menu($var) {
  return is_array($var) ? array_intersect($var, array(
        //List of allowed menu classes
        'current_page_item',
        'current_page_parent',
        'current_page_ancestor',
        'first',
        'last',
        'vertical',
        'horizontal'
        )
    ) : '';
}
add_filter('nav_menu_css_class', 'custom_wp_nav_menu');
add_filter('nav_menu_item_id', 'custom_wp_nav_menu');
add_filter('page_css_class', 'custom_wp_nav_menu');


//Replaces "current-menu-item" with "active"
function current_to_active($text){
    $replace = array(
        //List of menu item classes that should be changed to "active"
        'current_page_item' => 'active',
        'current_page_parent' => 'active',
        'current_page_ancestor' => 'active',
    );
    $text = str_replace(array_keys($replace), $replace, $text);
        return $text;
    }
add_filter ('wp_nav_menu','current_to_active');

//Deletes empty classes and removes the sub menu class
function strip_empty_classes($menu) {
    $menu = preg_replace('/ class=""| class="menu"/',' class="links-footer-ul"',$menu);
    return $menu;
}
add_filter ('wp_nav_menu','strip_empty_classes');

function site_logo(){
	$logo = get_field('site_main_logo',get_option('page_on_front')); 
	return $logo['url'];
}

function nav_menu_object_tree( $theme_location ) {
	
	$nav_menu_items_array = '';
	if ( ($theme_location) && ($locations = get_nav_menu_locations()) && isset($locations[$theme_location]) ) {
        $menu = get_term( $locations[$theme_location], 'nav_menu' );
        $nav_menu_items_array = wp_get_nav_menu_items($menu->term_id);
    }
    foreach ( $nav_menu_items_array as $key => $value ) {
        $value->children = array();
        $nav_menu_items_array[ $key ] = $value;
    }

    $nav_menu_levels = array();
    $index = 0;
    if ( ! empty( $nav_menu_items_array ) ) do {
        if ( $index == 0 ) {
            foreach ( $nav_menu_items_array as $key => $obj ) {
                if ( $obj->menu_item_parent == 0 ) {
                    $nav_menu_levels[ $index ][] = $obj;
                    unset( $nav_menu_items_array[ $key ] );
                }
            }
        } else {
            foreach ( $nav_menu_items_array as $key => $obj ) {
                if ( in_array( $obj->menu_item_parent, $last_level_ids ) ) {
                    $nav_menu_levels[ $index ][] = $obj;
                    unset( $nav_menu_items_array[ $key ] );
                }
            }
        }
        $last_level_ids = wp_list_pluck( $nav_menu_levels[ $index ], 'db_id' );
        $index++;
    } while ( ! empty( $nav_menu_items_array ) );

    $nav_menu_levels_reverse = array_reverse( $nav_menu_levels );

    $nav_menu_tree_build = array();
    $index = 0;
    if ( ! empty( $nav_menu_levels_reverse ) ) do {
        if ( count( $nav_menu_levels_reverse ) == 1 ) {
            $nav_menu_tree_build = $nav_menu_levels_reverse;
        }
        $current_level = array_shift( $nav_menu_levels_reverse );
        if ( isset( $nav_menu_levels_reverse[ $index ] ) ) {
            $next_level = $nav_menu_levels_reverse[ $index ];
            foreach ( $next_level as $nkey => $nval ) {
                foreach ( $current_level as $ckey => $cval ) {
                    if ( $nval->db_id == $cval->menu_item_parent ) {
                        $nval->children[] = $cval;
                    }
                }
            }
        }
    } while ( ! empty( $nav_menu_levels_reverse ) );

    $nav_menu_object_tree = $nav_menu_tree_build[ 0 ];
    return $nav_menu_object_tree;
}

function onec_custom_menu($theme_location){
	global $post;
	$arrayMenu = nav_menu_object_tree( $theme_location );
	$menu = '<ul class="nav navbar-nav list-inline">';
	foreach ( $arrayMenu as $okey => $obj ) {
		$active = ( strtolower($post->post_title ) == strtolower($obj->title)) ? "active" : "" ;
		$menu .= '<li class="'.$active.' '.$obj->classes[0].'">';
		$menu .= '<a href="'.$obj->url.'" title="'.$obj->title.'"><span>'.$obj->title.'</span></a>';
		// if($obj->children):
		// 	$menu .='<div class="menu sub-menu">';
		// 	$menu .='<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">';
  //           $menu .='<div class="service">';
  //           $menu .='<div class="row">';
		// 	//$menu .= "<ul>";
		// 	foreach ($obj->children as $ookey => $obj2) {
		// 		$menu .= '<div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">';
		// 		$menu .= '<div class="naw-list-items">';
		// 		$menu .= '<p class="name-sub-categry"><a href="'.$obj2->url.'" title="'.$obj2->title.'">'.$obj2->title.'</a></p>';
		// 		if($obj2->children):
		// 			$menu .= '<ul class="naw-ul">';
		// 			foreach ($obj2->children as $oookey => $obj3) {
		// 				$menu .= '<li class="hvr-sweep-to-right">';
		// 				$menu .= '<a href="'.$obj3->url.'" title="'.$obj3->title.'">'.$obj3->title.'</a>';
		// 				$menu .= '</li>';
		// 			}
		// 			$menu .= "</ul>";
		// 		endif;
		// 		$menu .= "</div>";
		// 		$menu .= '</div>';
		// 	}
		// 	//$menu .= "</ul>";
		// 	$menu .= "</div>";
		// 	$menu .= "</div>";
		// 	$menu .= "</div>";
		// 	$menu .= "</div>";
		// endif;
		$menu .= "</li>";
	}
	//get contact Number
 		$args = array(
    		'meta_key' => '_wp_page_template',
     		'meta_value' => 'page-templates/tpl_contactus.php'
			 );
 		$pages = get_pages( $args );
 		//$arrayVacancies = array();
 		//$arrayVacancies = get_field('current_vacancies',$pages[0]->ID);
		
   		 $contact_details= get_field('contact_details',$pages[0]->ID);
         	$tel= $contact_details[0]['number'];
         	//end get contact Number
	
	$menu .= '<li class="top-phone-icon hidden-xs"><a href="tel:+44'. $tel.'" title="+44'.$tel.'"><img src="'.get_template_directory_uri().'/images/top-phone.png" alt="phone-icon"></a></li>';
	$menu .= "</ul>";
	echo $menu;
}

function onec_footer_menu($theme_location){
	global $post;
	$arrayMenu = nav_menu_object_tree( $theme_location );
	$menu = '<ul class="list-style">';
	foreach ( $arrayMenu as $okey => $obj ) {
		$active = ( strtolower($post->post_title ) == strtolower($obj->title)) ? "active" : "" ;
		$menu .= '<li class="'.$active.' '.$obj->classes[0].'">';
		$menu .= '<a href="'.$obj->url.'" title="'.$obj->title.'">'.$obj->title.'</a>';
		$menu .= "</li>";
	}
	
	$menu .= "</ul>";
	echo $menu;
}

function site_main_slider(){
	global $post;
	$home_slider = get_field('home_slider_images',$post->ID); 
	return $home_slider;
}


// ---------------------------------

function getPageTitles(){
	global $post;
	$_title = get_field('page_title',$post->ID);
	$_title = ( trim( $_title ) == '' )? $post->post_title : $_title ;
	return   get_option( 'blogname' ).' | '.$_title ;
}

function setPageTitles(){
	global $post;
	$_title = get_field('page_title',$post->ID);
	return (  trim( $_title ) == '' )? $post->post_title : $_title ;
}

function set_description(){
	global $post;
	$_description = get_field('seo_description',$post->ID);
	return ( trim( $_description ) == '' )? '' : $_description ;
}

function set_keywords(){
	global $post;
	$_keywords = get_field('seo_keywords',$post->ID);
	return ( trim( $_keywords ) =='' )? '' : $_keywords ;
}

function set_header_tracking_codes()
{
	$_header_tracking = get_field('header_area',get_option('page_on_front'));
if($_header_tracking){
		foreach ($_header_tracking as $key => $value) {
			return ( trim( $value['header_tracking_codes'] ) =='' )? '' : $value['header_tracking_codes'] ;
		}
	//return ( trim( $_keywords ) =='' )? '' : $_keywords ;
	}
}

function set_body_tracking_codes()
{
	$_body_tracking = get_field('body_area',get_option('page_on_front'));
	if($_body_tracking ){
		foreach ($_body_tracking as $key => $value) {
			return ( trim( $value['body_tracking_codes'] ) =='' )? '' : $value['body_tracking_codes'] ;
		}
	//return ( trim( $_keywords ) =='' )? '' : $_keywords ;
	}
}


function getIdByTemplate($template = NULL){
	$args = array(
	    'meta_key' => '_wp_page_template',
	    'meta_value' => "page-templates/{$template}"
	);
	$pages = get_pages( $args );
	return $pages[0]->ID;
}

	
function getCustomFiledArray( $customFiled = NULL,$id=null ){
	global $post;
	$postid = $id;
	if (empty($postid)) {
		$postid = $post->ID;
	}
	$arrayCustomFiledArray = array();
	$arrayCustomFiledArray = get_field($customFiled,$postid);
	return $arrayCustomFiledArray;
}

// Site Map 
function _site_map(){
	$_args = array(
		"depth"        => 0,
		"show_date"    => "",
		"date_format"  => get_option("date_format"),
		"child_of"     => 0,
		"title_li"     => __(""),
		"echo"         => 0,
		"authors"      => "",
		"sort_column"  => "menu_order, post_title",
		"link_before"  => "",
		"link_after"   => "",
		"walker"       => "",
		"post_type"    => "page",
		"post_status"  => "publish",
		);
	echo "<ul class=\"sitemap\">";
	echo wp_list_pages($_args);
	echo "</ul>";
}

add_shortcode( 'SiteMap', '_site_map' );


function wpb_list_child_pages() { 
	global $post;
	$arrayPages = array();
	if ( is_page() && $post->post_parent ){
		$args = array(
			'post_parent' => $post->post_parent,
			'post_type'   => 'page', 
			'numberposts' => -1,
			'post_status' => 'publish',
			'order'          => 'ASC',
		    'orderby'        => 'menu_order' 
		);

		$children = new WP_Query( $args );
		if ( $children->have_posts() ) :
			$i=0;
		     while ( $children->have_posts() ) : $children->the_post();
		     	$id = get_the_id();
		     	$arrayPages[$i]['id'] = $id; 
		     	$arrayPages[$i]['title'] = get_the_title(); 
		        $arrayPages[$i]['permalink'] = get_the_permalink();
		        $i++; 
		 	endwhile; 
		endif;
		wp_reset_query(); 
	}

	// $currentElem = array_search($post->ID, $arrayPages);var_dump($currentElem);
	//$currentElem = array_search($post->ID, array_column($arrayPages, 'id'));
	//
	$arrayIds = array();
	foreach ($arrayPages as $key => $value) {
		$arrayIds[] = $value['id'];
	}

	$currentElem = array_search($post->ID, $arrayIds);

	$arrayNxtPrv['prv'] = ($currentElem - 1 >= 0) ? $arrayPages[ $currentElem - 1] :'';
	$arrayNxtPrv['nxt'] =  ($currentElem + 1 < count($arrayPages) ) ? $arrayPages[ $currentElem + 1] :'';;
	return $arrayNxtPrv;
}


function getSubPages(){
	global $post;

	$args = array(
		'post_parent' => $post->ID,
		'post_type'   => 'any', 
		'numberposts' => -1,
		'post_status' => 'publish',
		'order'          => 'ASC',
		'orderby'        => 'menu_order' 
	);
	$children = get_children( $args );
	$arrayPages = array();
	$i=0;
	foreach ($children as $key => $value) {
		$arrayPages[$i]['id'] = $value->ID;
		$arrayPages[$i]['title'] = $value->post_title;
		$arrayPages[$i]['excerpt'] = $value->post_excerpt;
		$arrayPages[$i]['permalink'] = get_the_permalink($value->ID);
		$arrayPages[$i]['image'] = wp_get_attachment_url( get_post_thumbnail_id( $value->ID ) );
		$i++; 
		
	}

	return $arrayPages;
}

// remove wp version param from any enqueued scripts
function remove_cssjs_ver( $src ) {
    if( strpos( $src, '?ver=' ) )
        $src = remove_query_arg( 'ver', $src );
    return $src;
}
add_filter( 'style_loader_src', 'remove_cssjs_ver', 10, 2 );
add_filter( 'script_loader_src', 'remove_cssjs_ver', 10, 2 );




function get_latest_post($noofpost=null)
{
	$args = array(
	'numberposts' => $noofpost,
	'offset' => 0,
	'category' => 'news',
	'orderby' => 'post_date',
	'order' => 'DESC',
	'include' => '',
	'exclude' => '',
	'meta_key' => '',
	'meta_value' =>'',
	'post_type' => 'post',
	'post_status' => 'publish',
	'suppress_filters' => true
);

$recent_posts = wp_get_recent_posts( $args, ARRAY_A );

return $recent_posts;
}

// Breadcrumbs
function custom_breadcrumbs() {
       
    // Settings
    $separator          = ' &nbsp';
    $breadcrums_id      = 'breadcrumbs';
    $breadcrums_class   = 'list-inline list-all-ul';
    $home_title         = 'Home';
      
    // If you have any custom post types with custom taxonomies, put the taxonomy name below (e.g. product_cat)
    $custom_taxonomy    = '';
       
    // Get the query & post information
    global $post,$wp_query;
       
    // Do not display on the homepage
    if ( !is_front_page() ) {
       
        // Build the breadcrums
        echo '<ul id="' . $breadcrums_id . '" class="' . $breadcrums_class . '">';
           
        // Home page
        echo '<li><a href="' . get_home_url() . '" title="' . $home_title . '">' . $home_title . '</a></li>';
        echo '<li> ' . $separator . ' </li>';
           
        if ( is_archive() && !is_tax() && !is_category() && !is_tag() ) {
              
            echo '<li class="item-current item-archive">' . post_type_archive_title($prefix, false) . '</li>';
              
        } else if ( is_archive() && is_tax() && !is_category() && !is_tag() ) {
              
            // If post is a custom post type
            $post_type = get_post_type();
              
            // If it is a custom post type display name and link
            if($post_type != 'post') {
                  
                $post_type_object = get_post_type_object($post_type);
                $post_type_archive = get_post_type_archive_link($post_type);
              
                echo '<li class="item-cat item-custom-post-type-' . $post_type . '"><a class="bread-cat bread-custom-post-type-' . $post_type . '" href="' . $post_type_archive . '" title="' . $post_type_object->labels->name . '">' . $post_type_object->labels->name . '</a></li>';
                echo '<li class="separator"> ' . $separator . ' </li>';
              
            }
              
            $custom_tax_name = get_queried_object()->name;
            echo '<li class="item-current item-archive">' . $custom_tax_name . '</li>';
              
        } else if ( is_single() ) {
              
            // If post is a custom post type
            $post_type = get_post_type();
              
            // If it is a custom post type display name and link
            if($post_type != 'post') {
                  
                $post_type_object = get_post_type_object($post_type);
                $post_type_archive = get_post_type_archive_link($post_type);
              
                echo '<li class="item-cat item-custom-post-type-' . $post_type . '"><a class="bread-cat bread-custom-post-type-' . $post_type . '" href="' . $post_type_archive . '" title="' . $post_type_object->labels->name . '">' . $post_type_object->labels->name . '</a></li>';
                echo '<li class="separator"> ' . $separator . ' </li>';
              
            }
              
            // Get post category info
            $category = get_the_category();
             
            if(!empty($category)) {
              
                // Get last category post is in
                $last_category = end(array_values($category));
                  
                // Get parent any categories and create array
                $get_cat_parents = rtrim(get_category_parents($last_category->term_id, true, ','),',');
                $cat_parents = explode(',',$get_cat_parents);
                  
                // Loop through parent categories and store in variable $cat_display
                $cat_display = '';
                foreach($cat_parents as $parents) {
                    $cat_display .= '<li class="item-cat">'.$parents.'</li>';
                    $cat_display .= '<li class="separator"> ' . $separator . ' </li>';
                }
             
            }
              
            // If it's a custom post type within a custom taxonomy
            $taxonomy_exists = taxonomy_exists($custom_taxonomy);
            if(empty($last_category) && !empty($custom_taxonomy) && $taxonomy_exists) {
                   
                $taxonomy_terms = get_the_terms( $post->ID, $custom_taxonomy );
                $cat_id         = $taxonomy_terms[0]->term_id;
                $cat_nicename   = $taxonomy_terms[0]->slug;
                $cat_link       = get_term_link($taxonomy_terms[0]->term_id, $custom_taxonomy);
                $cat_name       = $taxonomy_terms[0]->name;
               
            }
              
            // Check if the post is in a category
            if(!empty($last_category)) {
               //echo $cat_display;
            	//$news_url = site_url()."/news.html";
            	//echo '<li class="item-cat"><a href="'.$news_url.'" title="News">News </a></li>';
            	//echo '<li class="separator"> ' . $separator . ' </li>';
                echo '<li class="item-current item-' . $post->ID . '">' . get_the_title() . '</li>';
                  
            // Else if post is in a custom taxonomy
            } else if(!empty($cat_id)) {
                  
                echo '<li class="item-cat item-cat-' . $cat_id . ' item-cat-' . $cat_nicename . '"><a class="bread-cat bread-cat-' . $cat_id . ' bread-cat-' . $cat_nicename . '" href="' . $cat_name . '" title="' . $cat_name . '">' . $cat_name . '</a></li>';
                echo '<li class="separator"> ' . $separator . ' </li>';
                echo '<li class="item-current item-' . $post->ID . '">' . get_the_title() . '</li>';
              
            } else {
                  
                echo '<li class="item-current item-' . $post->ID . '">' . get_the_title() . '</li>';
                  
            }
              
        } else if ( is_category() ) {
               
            // Category page
            echo '<li class="item-current item-cat">' . single_cat_title('', false) . '</li>';
               
        } else if ( is_page() ) {
               
            // Standard page
            if( $post->post_parent ){
                   
                // If child page, get parents 
                $anc = get_post_ancestors( $post->ID );
                   
                // Get parents in the right order
                $anc = array_reverse($anc);
                   
                // Parent page loop
                if ( !isset( $parents ) ) $parents = null;
                foreach ( $anc as $ancestor ) {
                    $parents .= '<li class="item-parent item-parent-' . $ancestor . '"><a class="bread-parent bread-parent-' . $ancestor . '" href="' . get_permalink($ancestor) . '" title="' . get_the_title($ancestor) . '">' . get_the_title($ancestor) . '</a></li>';
                    $parents .= '<li class="separator separator-' . $ancestor . '"> ' . $separator . ' </li>';
                }
                   
                // Display parent pages
                echo $parents;
                   
                // Current page
                echo '<li class="item-current item-' . $post->ID . '"> ' . get_the_title() . '</li>';
                   
            } else {
                   
                // Just display current page if not parents
                echo '<li class="item-current item-' . $post->ID . '"> ' . get_the_title() . '</li>';
                   
            }
               
        } else if ( is_tag() ) {
               
            // Tag page
               
            // Get tag information
            $term_id        = get_query_var('tag_id');
            $taxonomy       = 'post_tag';
            $args           = 'include=' . $term_id;
            $terms          = get_terms( $taxonomy, $args );
            $get_term_id    = $terms[0]->term_id;
            $get_term_slug  = $terms[0]->slug;
            $get_term_name  = $terms[0]->name;
               
            // Display the tag name
            echo '<li class="item-current item-tag-' . $get_term_id . ' item-tag-' . $get_term_slug . '">' . $get_term_name . '</li>';
           
        } elseif ( is_day() ) {
               
            // Day archive
               
            // Year link
            echo '<li class="item-year item-year-' . get_the_time('Y') . '"><a class="bread-year bread-year-' . get_the_time('Y') . '" href="' . get_year_link( get_the_time('Y') ) . '" title="' . get_the_time('Y') . '">' . get_the_time('Y') . ' Archives</a></li>';
            echo '<li class="separator separator-' . get_the_time('Y') . '"> ' . $separator . ' </li>';
               
            // Month link
            echo '<li class="item-month item-month-' . get_the_time('m') . '"><a class="bread-month bread-month-' . get_the_time('m') . '" href="' . get_month_link( get_the_time('Y'), get_the_time('m') ) . '" title="' . get_the_time('M') . '">' . get_the_time('M') . ' Archives</a></li>';
            echo '<li class="separator separator-' . get_the_time('m') . '"> ' . $separator . ' </li>';
               
            // Day display
            echo '<li class="item-current item-' . get_the_time('j') . '"> ' . get_the_time('jS') . ' ' . get_the_time('M') . ' Archives</li>';
               
        } else if ( is_month() ) {
               
            // Month Archive
               
            // Year link
            echo '<li class="item-year item-year-' . get_the_time('Y') . '"><a class="bread-year bread-year-' . get_the_time('Y') . '" href="' . get_year_link( get_the_time('Y') ) . '" title="' . get_the_time('Y') . '">' . get_the_time('Y') . ' Archives</a></li>';
            echo '<li class="separator separator-' . get_the_time('Y') . '"> ' . $separator . ' </li>';
               
            // Month display
            echo '<li class="item-month item-month-' . get_the_time('m') . '">' . get_the_time('M') . ' Archives</li>';
               
        } else if ( is_year() ) {
               
            // Display year archive
            echo '<li class="item-current item-current-' . get_the_time('Y') . '">' . get_the_time('Y') . ' Archives</li>';
               
        } else if ( is_author() ) {
               
            // Auhor archive
               
            // Get the author information
            global $author;
            $userdata = get_userdata( $author );
               
            // Display author name
            echo '<li class="item-current item-current-' . $userdata->user_nicename . '">' . 'Author: ' . $userdata->display_name . '</li>';
           
        } else if ( get_query_var('paged') ) {
               
            // Paginated archives
            echo '<li class="item-current item-current-' . get_query_var('paged') . '">'.__('Page') . ' ' . get_query_var('paged') . '</li>';
               
        } else if ( is_search() ) {
           
            // Search results page
            echo '<li class="item-current item-current-' . get_search_query() . '">Search results for: ' . get_search_query() . '</li>';
           
        } elseif ( is_404() ) {
               
            // 404 page
            echo '<li>' . 'Error 404' . '</li>';
        }
       
        echo '</ul>';
           
    }
       
}


//get sub pages

function get_sub_pages()
{
	 global $post;
	$args = array(
		'post_parent' => $post->ID,
		'order' => 'ACS',
		'post_type'   => 'page', 
		'numberposts' => -1,
		'post_status' => 'publish' 
			);
	$children = get_children( $args );

return $children;
}

//remove admin bar

add_filter('show_admin_bar', '__return_false');

add_shortcode('setEditBar','setEditBar');

function setEditBar(){
 if (is_user_logged_in()) {
  
  $link .='<div style="position: fixed;top: 0;right: 15px;z-index: 9999;float: left">';
  $link .= '<a href="'.get_edit_post_link().'" title="Edit" style="color: red;text-decoration: none;display: block;padding: 5px 0;text-transform: uppercase;float: left;margin-right:15px;">';
  $link .= 'Edit';
  $link .= '</a>';
  $link .= '<a href="'.wp_logout_url().'" title="Log Out" style="color: red;text-decoration: none;display: block;padding: 5px 0;text-transform: uppercase;float: left;">';
  $link .= 'Log Out';
  $link .= '</a>';
  $link .='</div>';
  echo $link;
 }
}

//get parent 

function wps_parent_post(){
  global $post;
  if ($post->post_parent){
        $ancestors=get_post_ancestors($post->ID);
        $root=count($ancestors)-1;
        $parent = $ancestors[$root];
  } else {
        $parent = $post->ID;
  }
  if($post->ID != $parent){
      echo '<a href="'.get_permalink($post->post_parent).'" >Back to '.get_the_title($post->post_parent).'</a>';
  }
}




// numbered pagination
function pagination($pages = '', $range = 4)
{  
     $showitems = ($range * 2)+1;  
 
     global $paged;
     if(empty($paged)) $paged = 1;
 
     if($pages == '')
     {
         global $wp_query;
         $pages = $wp_query->max_num_pages;
         if(!$pages)
         {
             $pages = 1;
         }
     }   
 
     if(1 != $pages)
     {
         echo "<div class=\"pagination\"><span>Page ".$paged." of ".$pages."</span>";
         if($paged > 2 && $paged > $range+1 && $showitems < $pages) echo "<a href='".get_pagenum_link(1)."'>&laquo; First</a>";
         if($paged > 1 && $showitems < $pages) echo "<a href='".get_pagenum_link($paged - 1)."'>&lsaquo; Previous</a>";
 
         for ($i=1; $i <= $pages; $i++)
         {
             if (1 != $pages &&( !($i >= $paged+$range+1 || $i <= $paged-$range-1) || $pages <= $showitems ))
             {
                 echo ($paged == $i)? "<span class=\"current\">".$i."</span>":"<a href='".get_pagenum_link($i)."' class=\"inactive\">".$i."</a>";
             }
         }
 
         if ($paged < $pages && $showitems < $pages) echo "<a href=\"".get_pagenum_link($paged + 1)."\">Next &rsaquo;</a>";  
         if ($paged < $pages-1 &&  $paged+$range-1 < $pages && $showitems < $pages) echo "<a href='".get_pagenum_link($pages)."'>Last &raquo;</a>";
         echo "</div>\n";
     }
}



//==================  write all the codes before the minifier codes  ========================
// =============== html minifier code ============

class WP_HTML_Compression
{
    // Settings
	protected $compress_css = true;
	protected $compress_js = true;
	protected $info_comment = true;
	protected $remove_comments = true;

    // Variables
	protected $html;
	public function __construct($html)
	{
		if (!empty($html))
		{
			$this->parseHTML($html);
		}
	}
	public function __toString()
	{
		return $this->html;
	}
	protected function bottomComment($raw, $compressed)
	{
		$raw = strlen($raw);
		$compressed = strlen($compressed);

		$savings = ($raw-$compressed) / $raw * 100;

		$savings = round($savings, 2);

		return '<!--HTML compressed, size saved '.$savings.'%. From '.$raw.' bytes, now '.$compressed.' bytes-->';
	}
	protected function minifyHTML($html)
	{
		$pattern = '/<(?<script>script).*?<\/script\s*>|<(?<style>style).*?<\/style\s*>|<!(?<comment>--).*?-->|<(?<tag>[\/\w.:-]*)(?:".*?"|\'.*?\'|[^\'">]+)*>|(?<text>((<[^!\/\w.:-])?[^<]*)+)|/si';
		preg_match_all($pattern, $html, $matches, PREG_SET_ORDER);
		$overriding = false;
		$raw_tag = false;
   	 // Variable reused for output
		$html = '';
		foreach ($matches as $token)
		{
			$tag = (isset($token['tag'])) ? strtolower($token['tag']) : null;

			$content = $token[0];

			if (is_null($tag))
			{
				if ( !empty($token['script']) )
				{
					$strip = $this->compress_js;
				}
				else if ( !empty($token['style']) )
				{
					$strip = $this->compress_css;
				}
				else if ($content == '<!--wp-html-compression no compression-->')
				{
					$overriding = !$overriding;

   				 // Don't print the comment
					continue;
				}
				else if ($this->remove_comments)
				{
					if (!$overriding && $raw_tag != 'textarea')
					{
   					 // Remove any HTML comments, except MSIE conditional comments
						$content = preg_replace('/<!--(?!\s*(?:\[if [^\]]+]|<!|>))(?:(?!-->).)*-->/s', '', $content);
					}
				}
			}
			else
			{
				if ($tag == 'pre' || $tag == 'textarea')
				{
					$raw_tag = $tag;
				}
				else if ($tag == '/pre' || $tag == '/textarea')
				{
					$raw_tag = false;
				}
				else
				{
					if ($raw_tag || $overriding)
					{
						$strip = false;
					}
					else
					{
						$strip = true;

   					 // Remove any empty attributes, except:
   					 // action, alt, content, src
						$content = preg_replace('/(\s+)(\w++(?<!\baction|\balt|\bcontent|\bsrc)="")/', '$1', $content);

   					 // Remove any space before the end of self-closing XHTML tags
   					 // JavaScript excluded
						$content = str_replace(' />', '/>', $content);
					}
				}
			}

			if ($strip)
			{
				$content = $this->removeWhiteSpace($content);
			}

			$html .= $content;
		}

		return $html;
	}

	public function parseHTML($html)
	{
		$this->html = $this->minifyHTML($html);

		if ($this->info_comment)
		{
			$this->html .= "\n" . $this->bottomComment($html, $this->html);
		}
	}

	protected function removeWhiteSpace($str)
	{
		$str = str_replace("\t", ' ', $str);
		$str = str_replace("\n",  '', $str);
		$str = str_replace("\r",  '', $str);

		while (stristr($str, '  '))
		{
			$str = str_replace('  ', ' ', $str);
		}

		return $str;
	}
}

function wp_html_compression_finish($html)
{
	return new WP_HTML_Compression($html);
}

function wp_html_compression_start()
{
	ob_start('wp_html_compression_finish');
}
//add_action('get_header', 'wp_html_compression_start');

